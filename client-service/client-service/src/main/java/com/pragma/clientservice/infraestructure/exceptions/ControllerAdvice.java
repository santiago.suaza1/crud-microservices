package com.pragma.clientservice.infraestructure.exceptions;

import com.pragma.clientservice.infraestructure.exceptions.custom_exceptions.NotAvailableImageFormatException;
import com.pragma.clientservice.infraestructure.exceptions.custom_exceptions.NotSuchElementFoundException;
import com.pragma.clientservice.utils.SqlExceptionMessagesConverter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler({
            NotSuchElementFoundException.class, NotAvailableImageFormatException.class
    })
    public ResponseEntity<ErrorDto> handleNotSuchElementFound(ResponseStatusException exception){
        return new ResponseEntity<ErrorDto>(
                ErrorDto.builder().messageError(exception.getReason()).build(),
                exception.getStatus()
        );
    }

    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public ResponseEntity<String> handleSQLIntegrityConstraintViolation(SQLIntegrityConstraintViolationException exception){
        HashMap<String,String> reason = SqlExceptionMessagesConverter.parseConstraintViolated(exception.getMessage());
        return new ResponseEntity<>(
                "The "+attributeToString(reason.get("attribute"))+" must be unique",
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorDto> handleMethodArgumentNotValid(MethodArgumentNotValidException exception){
        BindingResult result = exception.getBindingResult();

        List<FieldError> fieldErrors = new ArrayList<>();
        if(result != null){
            fieldErrors = result.getFieldErrors();
        }
        StringBuilder message =  new StringBuilder();

        fieldErrors.forEach(field -> message.append(field.getField() + ": " + field.getDefaultMessage() + ". "));
        return new ResponseEntity<>(
                ErrorDto.builder().messageError(message.toString()).build(),
                HttpStatus.UNPROCESSABLE_ENTITY
        );
    }

    private String attributeToString(String attributeName){
        switch (attributeName){
            case "numero_identificacion": return "identification number";
            default: return null;
        }
    }

}

