package com.pragma.clientservice.domain.service;

import com.pragma.clientservice.application.dtos.input.ClientDto;
import com.pragma.clientservice.application.dtos.output.ProfileDto;
import com.pragma.clientservice.domain.repository.IClientRepository;
import com.pragma.clientservice.infraestructure.exceptions.custom_exceptions.NotSuchElementFoundException;
import lombok.AllArgsConstructor;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Transactional
public class ClientService {

    private IClientRepository clientRepository;

    public List<ClientDto> getAll(int age) {
        if (age!=0) return clientRepository.getByAge(age);
        else return clientRepository.getAll();
    }

    public ProfileDto getClient(int clientId){
        Optional<ClientDto> optClientDto = clientRepository.getClient(clientId);
        ProfileDto profileDto ;
        if (!optClientDto.isPresent()) throw new NotSuchElementFoundException("Not client found");
        else {
            profileDto = new ProfileDto();
            profileDto.setClientDto(optClientDto.get());
            profileDto.setImages(Arrays.asList("todas las imagenes")
            );
        }
        return profileDto;
    }

    public List<ClientDto> findByAge(int age){
        return clientRepository.getByAge(age);
    }

    public ClientDto save(ClientDto clientDto) {
        ClientDto savedClient = clientRepository.save(clientDto);
        return savedClient;
    }

    public ClientDto update(int clientId, ClientDto newInfoClient){
        ClientDto oldClientDto = getClient(clientId).getClientDto();

        ClientDto newClientDto = oldClientDto;
        newClientDto.setName(newInfoClient.getName());
        newClientDto.setLastName(newInfoClient.getLastName());
        newClientDto.setAge(newInfoClient.getAge());
        newClientDto.setCityOfBirth(newInfoClient.getCityOfBirth());
        return clientRepository.save(newClientDto);
    }

    public void delete(int clientId){
        getClient(clientId);
        clientRepository.delete(clientId);
    }
}
