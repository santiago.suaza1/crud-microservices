package com.pragma.clientservice.infraestructure.controller;

import com.pragma.clientservice.application.dtos.input.ClientDto;
import com.pragma.clientservice.application.dtos.output.ProfileDto;
import com.pragma.clientservice.application.handler.CrudClientHandler;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@Api(value = "/clients")
@RestController
@RequestMapping("/clients")
@Validated
public class ClientController {

    @Autowired
    private CrudClientHandler crudClientHandler;

    @GetMapping()
    @ApiOperation(value = "Get all clients", notes = "Returns all the users registered in the system.")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<List<ClientDto>> getAll(@RequestParam(name = "age", required = false, defaultValue = "0") int age){
        return new ResponseEntity<>(crudClientHandler.getAll(age), HttpStatus.OK);
    }

    @ApiOperation(value = "Search a client with an ID", notes = "Returns the info of an specified user")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Client not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<ProfileDto> getClient(@ApiParam(value = "The id of the client", required = true, example = "1")
                                                @PathVariable("id") int clientId){
        return new ResponseEntity<>(crudClientHandler.getClient(clientId), HttpStatus.OK);
    }

    @ApiOperation(value = "Register a client", notes = "Registry a client in the application")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
    })
    @PostMapping()
    public ResponseEntity<ProfileDto> save(@ApiParam(value = "Client info", required = true) @Valid @RequestPart("client") ClientDto client,
                                           @ApiParam(value = "Client's image", required = false)@RequestParam(value = "file", required = false)
                                                   MultipartFile photo) {
        return new ResponseEntity<>( crudClientHandler.save(client, photo), HttpStatus.CREATED);
    }

    @ApiOperation("Update client information")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
    })
    @PutMapping("/{id}")
    public ResponseEntity<ClientDto> update(@ApiParam(value = "Id of the client to be updated", required = true, example = "2") @PathVariable("id") int clientId,
                                            @ApiParam(value = "New info of the client", required = true) @RequestBody ClientDto newInfoClient){
        return new ResponseEntity<>(crudClientHandler.update(clientId, newInfoClient), HttpStatus.OK);
    }

    @ApiOperation("Delete a client from an ID")
    @ApiResponses({
            @ApiResponse(code = 204, message = "No_Content"),
            @ApiResponse(code = 404, message = "Not_Found"),
    })
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@ApiParam(value = "Id of the client to be deleted", required = true, example = "2") @PathVariable("id") int clientId){
        crudClientHandler.delete(clientId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

