package com.pragma.clientservice.application.dtos.output;

import com.pragma.clientservice.application.dtos.input.ClientDto;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProfileDto implements Serializable {
    ClientDto clientDto;
    List<String> images;
}
