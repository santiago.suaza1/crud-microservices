package com.pragma.clientservice.application.dtos.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@ApiModel("Client User")
public class ClientDto implements Serializable {
    @ApiModelProperty(example = "1", position = 0)
    private int clientId;

    @NotBlank(message = "Name cannot be null")
    @Size(min = 3, max = 20, message = "The name size must be between 5-20 characters")
    private String name;

    @NotBlank(message = "Last name cannot be null")
    @Size(min = 5, max = 20, message = "The last name size must be between 5-20 characters")
    private String lastName;

    @NotBlank(message = "Identification type cannot be null")
    private String identificationType;

    @NotBlank(message = "Identification number cannot be null")
    @Pattern(regexp = "^[0-9]{5,10}", message = "This field must have a minium of 5 numeric digits")
    private String identificationNumber;

    @ApiModelProperty(example = "22")
    @DecimalMin(value = "18", message = "The minimum age required is 18 years old")
    private int age;
    private String cityOfBirth;
}

