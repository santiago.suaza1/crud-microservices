package com.pragma.clientservice.domain.repository;

import com.pragma.clientservice.application.dtos.input.ClientDto;

import java.util.List;
import java.util.Optional;

public interface IClientRepository {
    List<ClientDto> getAll();
    List<ClientDto> getByAge(int age);
    Optional<ClientDto> getClient(int ClientId);
    ClientDto save(ClientDto client);
    void delete(int ClientId);
}
