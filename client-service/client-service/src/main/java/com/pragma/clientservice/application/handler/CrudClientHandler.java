package com.pragma.clientservice.application.handler;

import com.pragma.clientservice.application.dtos.image.ImageDto;
import com.pragma.clientservice.application.dtos.image.InputImageDto;
import com.pragma.clientservice.application.dtos.input.ClientDto;
import com.pragma.clientservice.application.dtos.output.ProfileDto;
import com.pragma.clientservice.domain.repository.IClientRepository;
import com.pragma.clientservice.domain.service.ClientService;
import com.pragma.clientservice.infraestructure.exceptions.custom_exceptions.NotAvailableImageFormatException;
import com.pragma.clientservice.infraestructure.feingclients.ImageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
public class CrudClientHandler {

    @Autowired
    private ClientService clientService;

    @Autowired
    private IClientRepository clientRepository;

    @Autowired
    private ImageFeignClient imageFeignClient;

    public List<ClientDto> getAll(int age){
        return clientService.getAll(age);
    }

    public ProfileDto getClient(int clientId){
        return clientService.getClient(clientId);
    }

    public List<ClientDto> findByAge(int age){
        return clientService.findByAge(age);
    }

    public ProfileDto save(ClientDto clientDto, MultipartFile photo) {
        String photoOnBytesString = "";
        InputImageDto inputImageDto = new InputImageDto();
        ImageDto imageDto = null;
        ProfileDto profileDto = new ProfileDto();

        ClientDto savedClient = clientService.save(clientDto);
        if(photo != null) {
            try {
                photoOnBytesString = photo.getBytes().toString();
            } catch (IOException ex) {
                throw new NotAvailableImageFormatException("Client image format not accepted");
            }
            inputImageDto.setClientId(String.valueOf(savedClient.getClientId()));
            inputImageDto.setPhoto(photoOnBytesString);
            inputImageDto.setUserName(clientDto.getName());
            inputImageDto.setType("JPG");

            imageDto = imageFeignClient.saveFromNewClient(inputImageDto);
            profileDto.setImages(Arrays.asList(imageDto.getPhoto()));
        }

        profileDto.setClientDto(savedClient);

        return profileDto;
    }

    public ClientDto update(int clientId, ClientDto newInfoClient){
        return clientService.update(clientId, newInfoClient);
    }

    public void delete(int clientId){
        clientService.delete(clientId);
    }
}
