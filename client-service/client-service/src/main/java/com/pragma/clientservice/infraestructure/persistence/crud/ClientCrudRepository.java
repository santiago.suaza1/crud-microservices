package com.pragma.clientservice.infraestructure.persistence.crud;

import com.pragma.clientservice.domain.entity.Client;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClientCrudRepository extends CrudRepository<Client, Integer> {
    List<Client> findByEdad(int age);
    Client findByNumeroIdentificacion(String numIdentificacion);
}
