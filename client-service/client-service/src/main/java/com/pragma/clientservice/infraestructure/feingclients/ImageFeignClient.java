package com.pragma.clientservice.infraestructure.feingclients;

import com.pragma.clientservice.application.dtos.image.ImageDto;
import com.pragma.clientservice.application.dtos.image.InputImageDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "IMAGE-SERVICE", url = "http://localhost:9002")
public interface ImageFeignClient {

    @RequestMapping(value = "/images", method = RequestMethod.POST)
    ImageDto saveFromNewClient(@RequestBody InputImageDto inputImageDto);
}
