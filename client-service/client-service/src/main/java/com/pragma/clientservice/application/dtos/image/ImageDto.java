package com.pragma.clientservice.application.dtos.image;

import lombok.Data;

import java.io.Serializable;

@Data
public class ImageDto implements Serializable {
    private String imageId;
    private String type;
    private String name;
    private String photo;
    private String clientId;
}
