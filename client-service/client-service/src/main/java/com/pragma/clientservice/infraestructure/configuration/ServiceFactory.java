package com.pragma.clientservice.infraestructure.configuration;


import com.pragma.clientservice.domain.repository.IClientRepository;
import com.pragma.clientservice.domain.service.ClientService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceFactory {

    @Bean
    public ClientService clientService(IClientRepository clientRepository){
        return new ClientService(clientRepository);
    }
}

