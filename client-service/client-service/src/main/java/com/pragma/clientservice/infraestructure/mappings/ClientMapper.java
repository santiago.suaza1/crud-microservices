package com.pragma.clientservice.infraestructure.mappings;

import com.pragma.clientservice.application.dtos.input.ClientDto;
import com.pragma.clientservice.domain.entity.Client;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "Spring") //, uses = {ImageMapper.class}
public interface ClientMapper {
        @Mappings({
                @Mapping(source = "id", target = "clientId"),
                @Mapping(source = "nombre", target = "name"),
                @Mapping(source = "apellidos", target = "lastName"),
                @Mapping(source = "tipoIdentificacion", target = "identificationType"),
                @Mapping(source = "numeroIdentificacion", target = "identificationNumber"),
                @Mapping(source = "edad", target = "age"),
                @Mapping(source = "ciudadNacimiento", target = "cityOfBirth"),
                //@Mapping(source = "imagen", target = "image")
        })
        ClientDto toClientDto(Client client);
        List<ClientDto> toClientsDto(List<Client> clients);

        @InheritInverseConfiguration
        Client toClient(ClientDto clientDto);
}
