package com.pragma.clientservice.infraestructure.persistence;

import com.pragma.clientservice.application.dtos.input.ClientDto;
import com.pragma.clientservice.domain.entity.Client;
import com.pragma.clientservice.domain.repository.IClientRepository;
import com.pragma.clientservice.infraestructure.mappings.ClientMapper;
import com.pragma.clientservice.infraestructure.persistence.crud.ClientCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ClientRepository implements IClientRepository {

    @Autowired
    private ClientCrudRepository clientCrudRepository;

    @Autowired
    private ClientMapper mapper;

    @Override
    public List<ClientDto> getAll() {
        List<Client> clients = (List<Client>) clientCrudRepository.findAll();
        return mapper.toClientsDto(clients);
    }

    @Override
    public List<ClientDto> getByAge(int edad) {
        return mapper.toClientsDto(clientCrudRepository.findByEdad(edad));
    }

    @Override
    public Optional<ClientDto> getClient(int clientId) {
        return clientCrudRepository.findById(clientId).map(client -> mapper.toClientDto(client));
    }

    @Override
    public ClientDto save(ClientDto clientDto) {
        Client client = mapper.toClient(clientDto);
        Client clientSaved;
        clientSaved = clientCrudRepository.save(client);

        return mapper.toClientDto(clientSaved);
    }

    @Override
    public void delete(int clientId) {
        clientCrudRepository.deleteById(clientId);
    }

}
