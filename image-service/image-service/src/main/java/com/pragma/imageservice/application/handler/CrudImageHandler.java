package com.pragma.imageservice.application.handler;

import com.pragma.imageservice.application.DTOs.ImageDto;
import com.pragma.imageservice.application.DTOs.InputImageDto;
import com.pragma.imageservice.domain.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CrudImageHandler {

    @Autowired
    private ImageService imageService;

    public List<ImageDto> getAll(){
        return imageService.getAll();
    }

    public ImageDto getImage(String imageId){
        return imageService.getImage(imageId);
    }

    public ImageDto save(ImageDto image){
        return imageService.save(image);
    }

    public ImageDto saveFromNewClient(InputImageDto inputImageDto){
        return imageService.save(
                inputImageDto.getPhoto(),
                inputImageDto.getUserName(),
                Integer.parseInt(inputImageDto.getClientId()));
    }

    public ImageDto update(String imageId, ImageDto newInfoImage){
        return imageService.update(imageId, newInfoImage);
    }

    public List<ImageDto> getByClientId(int clientId){
        return imageService.getByClientId(clientId);
    }

    public void delete(String imageId){
        imageService.delete(imageId);
    }
}

