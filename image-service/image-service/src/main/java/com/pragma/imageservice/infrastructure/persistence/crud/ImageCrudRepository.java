package com.pragma.imageservice.infrastructure.persistence.crud;

import com.pragma.imageservice.domain.entity.Image;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ImageCrudRepository extends MongoRepository<Image, String> {
    List<Image> findByIdCliente(String idCliente);
}
