package com.pragma.imageservice.infrastructure.exceptions;

import com.pragma.imageservice.infrastructure.exceptions.custom_exceptions.NotAvailableImageFormatException;
import com.pragma.imageservice.infrastructure.exceptions.custom_exceptions.NotSuchElementFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;

import java.util.List;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler({
            NotSuchElementFoundException.class, NotAvailableImageFormatException.class
    })
    public ResponseEntity<ErrorDto> handleNotSuchElementFound(ResponseStatusException exception){
        return new ResponseEntity<ErrorDto>(
                ErrorDto.builder().messageError(exception.getReason()).build(),
                exception.getStatus()
        );
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorDto> handleMethodArgumentNotValid(MethodArgumentNotValidException exception){
        BindingResult result = exception.getBindingResult();

        List<FieldError> fieldErrors = new ArrayList<>();
        if(result != null){
            fieldErrors = result.getFieldErrors();
        }
        StringBuilder message =  new StringBuilder();

        fieldErrors.forEach(field -> message.append(field.getField() + ": " + field.getDefaultMessage() + ". "));
        return new ResponseEntity<>(
                ErrorDto.builder().messageError(message.toString()).build(),
                HttpStatus.UNPROCESSABLE_ENTITY
        );
    }
}
