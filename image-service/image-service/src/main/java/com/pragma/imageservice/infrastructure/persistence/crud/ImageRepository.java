package com.pragma.imageservice.infrastructure.persistence.crud;

import com.pragma.imageservice.application.DTOs.ImageDto;
import com.pragma.imageservice.domain.entity.Image;
import com.pragma.imageservice.domain.repository.IImageRepository;
import com.pragma.imageservice.infrastructure.mappings.ImageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ImageRepository implements IImageRepository {

    @Autowired
    private ImageCrudRepository imageCrudRepository;

    @Autowired
    private ImageMapper mapper;

    @Override
    public List<ImageDto> getAll(){
        return mapper.toImagesDto((List<Image>) imageCrudRepository.findAll());
    }

    @Override
    public Optional<ImageDto> getImage(String imageId) {
        return imageCrudRepository.findById(imageId).map(image -> mapper.toImageDto(image));
    }

    @Override
    public ImageDto save(ImageDto image) {
        return mapper.toImageDto(imageCrudRepository.save(mapper.toImage(image)));
    }

    @Override
    public List<ImageDto> getByClientId(String clientId) {
        return mapper.toImagesDto(imageCrudRepository.findByIdCliente(clientId));
    }

    @Override
    public void delete(String imageId) {
        imageCrudRepository.deleteById(imageId);
    }
}
