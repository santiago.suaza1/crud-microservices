package com.pragma.imageservice.infrastructure.exceptions;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorDto{
    private String messageError;
}
