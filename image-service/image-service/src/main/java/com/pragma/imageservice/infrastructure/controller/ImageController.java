package com.pragma.imageservice.infrastructure.controller;

import com.pragma.imageservice.application.DTOs.ImageDto;
import com.pragma.imageservice.application.DTOs.InputImageDto;
import com.pragma.imageservice.application.handler.CrudImageHandler;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/images")
public class ImageController {

    @Autowired
    private CrudImageHandler crudImageHandler;

    @ApiOperation("Get all images of the clients")
    @ApiResponse(code = 200, message = "OK")
    @GetMapping()
    public ResponseEntity<List<ImageDto>> getAll(){
        return new ResponseEntity<>(crudImageHandler.getAll(), HttpStatus.OK);
    }

    @ApiOperation("Search a Image with an ID")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Image not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<ImageDto> getImage(@ApiParam(value = "The ID of an image", required = true) @PathVariable("id") String imageId){
        return new ResponseEntity<>(crudImageHandler.getImage(imageId), HttpStatus.OK);
    }

    /*@ApiOperation("Save an Image")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
    })
    @PostMapping()
    public ResponseEntity<ImageDto> save(@ApiParam(value = "Image from a Client", required = true) @RequestBody ImageDto image){
        return new ResponseEntity<>(crudImageHandler.save(image), HttpStatus.CREATED);
    }*/

    @ApiOperation("Save an Image from a new client")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
    })
    @PostMapping
    public ResponseEntity<ImageDto> saveFromNewClient(@RequestBody InputImageDto inputImageDto){
        return new ResponseEntity<>(crudImageHandler.saveFromNewClient(inputImageDto), HttpStatus.CREATED);
    }

    @ApiOperation("Update image information")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
    })
    @PutMapping("/{id}")
    public ResponseEntity<ImageDto> update(@ApiParam(value = "Image ID to be updated", required = true) @PathVariable("id") String imageId ,
                                           @ApiParam(value = "New info of the image", required = true) @RequestBody ImageDto newImageInfo){
        return new ResponseEntity<>(crudImageHandler.update(imageId, newImageInfo), HttpStatus.OK);
    }

    @ApiOperation("Delete a image from an ID")
    @ApiResponses({
            @ApiResponse(code = 204, message = "No_Content"),
            @ApiResponse(code = 404, message = "Not_Found"),
    })
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@ApiParam(value = "ID of the image to be deleted", required = true) @PathVariable("id") String imageId){
        crudImageHandler.delete(imageId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

