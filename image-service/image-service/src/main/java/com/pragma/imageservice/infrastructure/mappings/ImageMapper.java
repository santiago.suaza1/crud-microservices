package com.pragma.imageservice.infrastructure.mappings;

import com.pragma.imageservice.application.DTOs.ImageDto;
import com.pragma.imageservice.domain.entity.Image;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ImageMapper {
    @Mappings({
            @Mapping(source = "id", target = "imageId"),
            @Mapping(source = "tipo", target = "type"),
            @Mapping(source = "nombre", target = "name"),
            @Mapping(source = "foto", target = "photo"),
            @Mapping(source = "idCliente", target = "clientId")
    })
    ImageDto toImageDto(Image imagen);
    List<ImageDto> toImagesDto(List<Image> imagenes);

    @InheritInverseConfiguration
    Image toImage(ImageDto image);
}
