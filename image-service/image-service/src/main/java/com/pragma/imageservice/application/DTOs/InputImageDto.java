package com.pragma.imageservice.application.DTOs;

import lombok.Data;

import java.io.Serializable;

@Data
public class InputImageDto implements Serializable{
    private String type;
    private String userName;
    private String photo;
    private String clientId;
}
