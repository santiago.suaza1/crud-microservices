package com.pragma.imageservice.infrastructure.configuration;

import com.pragma.imageservice.domain.repository.IImageRepository;
import com.pragma.imageservice.domain.service.ImageService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceFactory {
    @Bean
    public ImageService imageService(IImageRepository imageRepository){
        return new ImageService(imageRepository);
    }
}
