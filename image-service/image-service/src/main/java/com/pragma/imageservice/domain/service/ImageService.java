package com.pragma.imageservice.domain.service;

import com.pragma.imageservice.application.DTOs.ImageDto;
import com.pragma.imageservice.domain.repository.IImageRepository;
import com.pragma.imageservice.infrastructure.exceptions.custom_exceptions.NotSuchElementFoundException;
import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
@Transactional
public class ImageService {

    private IImageRepository imageRepository;

    public List<ImageDto> getAll() {
        return imageRepository.getAll();
    }

    public ImageDto getImage(String imageId) {
        Optional<ImageDto> optImageDto = imageRepository.getImage(imageId);
        if (optImageDto.isPresent()) return optImageDto.get();
        else throw new NotSuchElementFoundException("Not image with that id found");
    }

    public ImageDto save(ImageDto image) {
        return imageRepository.save(image);
    }

    public ImageDto save(String photoOnBytesString, String userName, int clientId) {
        ImageDto imageDto = new ImageDto();
        imageDto.setPhoto(photoOnBytesString);
        imageDto.setClientId(String.valueOf(clientId));
        imageDto.setName(userName + UUID.randomUUID());

        return imageRepository.save(imageDto);
    }

    public ImageDto update(String imageId, ImageDto newInfoImage) {
        ImageDto oldImageDto = getImage(imageId);
        ImageDto newImageDto = oldImageDto;
        newImageDto.setType(newInfoImage.getType());
        newImageDto.setName(newInfoImage.getName());
        newImageDto.setPhoto(newInfoImage.getPhoto());
        newImageDto.setClientId(newInfoImage.getClientId());
        return imageRepository.save(newImageDto);
    }

    public List<ImageDto> getByClientId(int clientId) {
        return imageRepository.getByClientId(String.valueOf(clientId));
    }

    public void delete(String imageId) {
        getImage(imageId);
        imageRepository.delete(imageId);
    }

    public void deleteImagesFromClient(int clientId) {
        for (ImageDto image : getByClientId(clientId)) delete(image.getImageId());
    }
}
