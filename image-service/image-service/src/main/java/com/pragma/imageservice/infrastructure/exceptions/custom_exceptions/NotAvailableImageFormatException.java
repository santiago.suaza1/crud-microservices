package com.pragma.imageservice.infrastructure.exceptions.custom_exceptions;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class NotAvailableImageFormatException extends ResponseStatusException{

    public NotAvailableImageFormatException(String reason) {
        super(HttpStatus.BAD_REQUEST, reason);
    }
}

