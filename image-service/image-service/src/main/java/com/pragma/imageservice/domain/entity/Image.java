package com.pragma.imageservice.domain.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("imagenes")
@Data
public class Image{

    @Id
    private String id;

    private String tipo;

    private String nombre;

    private String foto;

    private String idCliente;
}

