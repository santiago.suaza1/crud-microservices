package com.pragma.imageservice.domain.repository;

import com.pragma.imageservice.application.DTOs.ImageDto;

import java.util.List;
import java.util.Optional;

public interface IImageRepository {
    List<ImageDto> getAll();
    Optional<ImageDto> getImage(String imageId);
    ImageDto save(ImageDto image);
    List<ImageDto> getByClientId(String idCliente);
    void delete(String imageId);
}
